<?php

return [
    'url_to_openapi' => '/storage/api.json',
    'title' => 'Swagger UI',
    'version' => '1',
    'description' => 'description',
    'api_prefix' => 'api',
    'json_file' => 'public/api.json',
    'has_auth' => true
];
